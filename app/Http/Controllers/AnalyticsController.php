<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Http\Requests;
use App\Item;
use App\Image;
use App\Log;
use App\Branch;
use App\Status;
use App\Seller;
use Auth;

class AnalyticsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{

		$filters = [];

		$reportType = '';

		if ($request->has('brand')) {
			array_push($filters, ['brand', '=', $request->brand]);
		}
		if ($request->has('stored_in')) {
			array_push($filters, ['stored_in', '=', $request->stored_in]);
		}
		if ($request->has('reportType') && $request->reportType != 'Sales') {
			if ($request->reportType == 'New Items') {
				array_push($filters, ['status', '=', 'Available']);
			}
			if ($request->reportType == 'Lay Away') {
				array_push($filters, ['status', '=', 'Lay Away']);
			}
			$reportType = $request->reportType;
			//array_push($filters, ['reportType', '=', $request->reportType]);
		}
		else {
			array_push($filters, ['status', '=', 'Sold']);
			$reportType = 'Sold';
		}
		if ($request->has('category')) {
			array_push($filters, ['category', '=', $request->category]);
		}
		if ($request->has('s')) {
			array_push($filters, ['name', 'LIKE', '%'.$request->s.'%']);
		}
		if ($request->has('date_start')) {
			if ($reportType == 'Sold') {
				array_push( $filters, [ 'date_sold', '>=', $request->date_start ] );
			}
			if ($request->reportType == 'New Items') {
				array_push( $filters, [ 'created_at', '>=', $request->date_start ] );
			}
			if ($request->reportType == 'Lay Away') {
				array_push( $filters, [ 'date_layaway', '>=', $request->date_start ] );
			}
		}
		if ($request->has('date_end')) {
			if ($reportType == 'Sold') {
				array_push( $filters, [ 'date_sold', '<=', $request->date_end ] );
			}
			if ($request->reportType == 'New Items') {
				array_push( $filters, [ 'created_at', '<=', $request->date_end ] );
			}
			if ($request->reportType == 'Lay Away') {
				array_push( $filters, [ 'date_layaway', '<=', $request->date_end ] );
			}
		}


		if ($reportType == 'Sold') {
			$items =  Item::where($filters)->orderBy('date_sold','desc')->get();
		}
		if ($reportType == 'New Items') {
			$items =  Item::where($filters)->orderBy('created_at','desc')->get();
		}
		if ($reportType == 'Lay Away') {
			$items =  Item::where($filters)->orderBy('date_layaway','desc')->get();
		}

		$statuses = Status::all();

		$categories = Item::orderBy('category', 'asc')->groupBy('category')->get()->reject(function ($v) { return is_null($v->category) || $v->category == ''; });
		$types = Item::orderBy('type', 'asc')->groupBy('type')->get()->reject(function ($v) { return is_null($v->type) || $v->type == ''; });
		$branches = Item::orderBy('stored_in', 'asc')->groupBy('stored_in')->get()->reject(function ($v) { return is_null($v->stored_in) || $v->stored_in == ''; });
		$brands = Item::orderBy('brand', 'asc')->groupBy('brand')->get()->reject(function ($v) { return is_null($v->brand) || $v->brand == ''; })->unique();

		return view('analytics', compact(['items', 'branches', 'sellers', 'brands', 'statuses', 'request', 'categories', 'types', 'reportType']));
	}

	public function update_date_sold(Request $request) {
		$logFilter = [];
		array_push($logFilter, ['entry', 'LIKE', '%Sold%']);

		$logs = Log::where($logFilter)->orderBy('updated_at','desc')->get()->unique('item_id');

		$itemsIdArray = $logs->lists('item_id')->toArray();

		$items = Item::find($itemsIdArray);

		foreach($logs as $log) {
			$item = Item::find($log->item_id);
			$item->date_sold = $log->updated_at;
			$item->save();
		}

		return 'Success';

	}
	public function update_date_layaway(Request $request) {
		$logFilter = [];
		array_push($logFilter, ['entry', 'LIKE', '%Lay Away%']);

		$logs = Log::where($logFilter)->orderBy('updated_at','desc')->get()->unique('item_id');

		$itemsIdArray = $logs->lists('item_id')->toArray();

		$items = Item::find($itemsIdArray);

		foreach($logs as $log) {
			$item = Item::find($log->item_id);
			$item->date_layaway = $log->updated_at;
			$item->save();
		}

		return 'Success';

	}


}
