<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use App\Http\Requests;
use App\Item;
use App\Image;
use App\Log;
use App\Branch;
use App\Status;
use App\Seller;
use Auth;

class InventoryController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$filters = [];
		
		if ($request->has('brand')) {
			array_push($filters, ['brand', '=', $request->brand]);
		}
		if ($request->has('stored_in')) {
			array_push($filters, ['stored_in', '=', $request->stored_in]);
		}
		if ($request->has('status')) {
			array_push($filters, ['status', '=', $request->status]);
		}
		if ($request->has('category')) {
			array_push($filters, ['category', '=', $request->category]);
		}
		if ($request->has('s')) {
			array_push($filters, ['name', 'LIKE', '%'.$request->s.'%']);
		}
		
		$items =  Item::with(['images'])->where($filters)->orderBy('created_at','desc')->paginate(25);
		
		$statuses = Status::all();

		$categories = Item::orderBy('category', 'asc')->groupBy('category')->get()->reject(function ($v) { return is_null($v->category) || $v->category == ''; });
		$types = Item::orderBy('type', 'asc')->groupBy('type')->get()->reject(function ($v) { return is_null($v->type) || $v->type == ''; });
		$branches = Item::orderBy('stored_in', 'asc')->groupBy('stored_in')->get()->reject(function ($v) { return is_null($v->stored_in) || $v->stored_in == ''; });
		$brands = Item::orderBy('brand', 'asc')->groupBy('brand')->get()->reject(function ($v) { return is_null($v->brand) || $v->brand == ''; })->unique();
		
		return view('inventory.index', compact(['items', 'branches', 'sellers', 'brands', 'statuses', 'request', 'categories', 'types']));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$item = new Item;
		return view('inventory.create', compact('item'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		
		/*
		$validator = Validator::make($request->all(), [
			'name' => 'required|max:255',
		]);

		if ($validator->fails()) {
			return redirect('/')
				->withInput()
				->withErrors($validator);
		}
		*/

		$item = new Item;
		$item->name = $request->name;
		$item->type = $request->type;
		$item->brand = $request->brand;
		$item->color = $request->color;
		$item->stored_in = $request->stored_in;
		$item->sold_by = $request->sold_by;
		$item->status = $request->status;
		$item->date_added = $request->date_added;
		$item->date_sold = $request->date_sold;
		$item->brand_new = $request->brand_new;
		$item->price = $request->price;
		$item->save();
		
		$log = new Log;
		$log->item_id = $item->id;
		$log->entry = "Added to the inventory by " . Auth::user()->name;
		$log->save();

		return redirect('/inventory');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$item = Item::findOrFail($id);
		return view('inventory.edit', compact(['item']));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		// TODO: Add logging for dirty keys
		if (Auth::user()->can('manage_items')) {
			$item             = Item::findOrFail( $id );
			$item->name       = $request->name;
			$item->category   = $request->category;
			$item->type       = $request->type;
			$item->brand      = $request->brand;
			$item->color      = $request->color;
			$item->stored_in  = $request->stored_in;
			$item->sold_by    = $request->sold_by;
			$item->brand_new  = $request->brand_new;
			$item->price      = $request->price;
			$item->notes      = $request->notes;
			$item->save();

			$log = new Log;
			$log->item_id = $item->id;
			$log->entry = "Edited by " . Auth::user()->name;
			$log->save();

			return redirect( '/inventory' );
		}
	}
	
	public function update_key(Request $request)
	{
		if (Auth::user()->can('manage_items')) {
			$item = Item::findOrFail($request->id);
			$oldvalue = $item[$request->key];
			$item[$request->key] = $request->value;
			$item->save();

			$log = new Log;
			$log->item_id = $item->id;
			$log->entry = '<strong>' . $request->key . '</strong> has been updated from ' . $oldvalue .'  to <strong>' . $request->value . '</strong> by ' . Auth::user()->name;
			$log->save();

			return response()->json(['success' => 'true']);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if (Auth::user()->can('manage_users')) {
			$item = Item::find( $id );

			$item->delete();

			return redirect( '/inventory' );
		}
	}
	
	public function update_sold(Request $request)
	{
		if (Auth::user()->can('manage_items')) {
			$item             = Item::findOrFail( $request->id );
			$item->sold_by    = $request->sold_by;
			$item->date_sold  = Carbon::now();
			$item->notes      = $request->notes;
			$item->save();

			$log = new Log;
			$log->item_id = $item->id;
			$log->entry = "Marked as sold by " . Auth::user()->name;
			$log->save();

			return redirect( '/inventory' );
		}

	}

	public function update_layaway(Request $request)
	{
		if (Auth::user()->can('manage_items')) {
			$item             = Item::findOrFail( $request->id );
			$item->date_layaway  = Carbon::now();
			$item->notes      = $request->notes;
			$item->save();

			$log = new Log;
			$log->item_id = $item->id;
			$log->entry = "Marked as layaway by " . Auth::user()->name;
			$log->save();

			return redirect( '/inventory' );
		}

	}

	public function batch_upload(Request $request) 
	{
		$json = $request->items;
		
		foreach ($json as $j) {
			$item = new Item;
			if (array_key_exists('Description', $j)) {
				$item->name = trim( $j['Description'] );
			}
			if (array_key_exists('Category', $j)) {
				$item->category = trim( $j['Category'] );
			}
			if (array_key_exists('Brand', $j)) {
				$item->brand = trim( $j['Brand'] );
			}
			if (array_key_exists('Type', $j)) {
				$item->type = trim($j['Type']);
			}
			if (array_key_exists('Color', $j)) {
				$item->color = trim($j['Color']);
			}
			if (array_key_exists('Price', $j)) {
				$item->price = str_replace(',', '', trim($j['Price']));
			}
			
			if (array_key_exists('Brandnew', $j)) 
			{
				$j['Brandnew'] = trim($j['Brandnew']);
				
				if ($j['Brandnew'] == 'New') 
				{
					$item->brand_new = true;
				}
				else if ($j['Brandnew'] == 'Preowned')  
				{
					$item->brand_new = false;
				}
			}
			
			if (array_key_exists('Accessories', $j)) {
				$item->accessories = $j['Accessories'];
			}
			if (array_key_exists('Branch', $j)) {
				$item->stored_in = trim($j['Branch']);
			}
			if (array_key_exists('Sold to', $j)) {
				$item->sold_to = trim($j['Sold to']);
			}
			if (array_key_exists('Notes', $j)) {
				$item->notes = trim($j['Notes']);
			}
			if (array_key_exists('Gender', $j)) {
				$item->gender = trim($j['Gender']);
			}
			if (array_key_exists('Date Added', $j)) {
				$date = \DateTime::createFromFormat('m/d/y', trim($j['Date Added']))->format('Y-m-d');
				$item->date_added = $date;
			}
			if (array_key_exists('Date Sold', $j)) {
				$date = \DateTime::createFromFormat('m/d/y', trim($j['Date Sold']))->format('Y-m-d');
				$item->date_sold = $date;
			}
			if (array_key_exists('Style', $j)) {
				$item->style = trim($j['Style']);
			}
			if (array_key_exists('Size', $j)) {
				$item->size = trim($j['Size']);
			}
			
			if (array_key_exists('Sold to', $j) && $j['Sold to']) {
				$item->status = 'Sold';
			}
			else {
				$item->status = 'Available';
			}

			
			$item->save();
			
			$log = new Log;
			$log->item_id = $item->id;
			$log->entry = "Added to the inventory by " . Auth::user()->name;
			$log->save();
			
		}

		return response()->json(['success' => 'true']);

	}

	public function upload_image(Request $request)
	{
		if (Auth::user()->can('upload_photo')) {
			if ( $request->hasFile( 'file' ) ) {

				$item = Item::findOrFail( $request->id );


				$destinationPath = 'uploads';

				// Normalize filename

				$itemName = preg_replace( '/[^a-zA-Z0-9-_\.]/', '', $item->name ) . '-' . str_random( 10 );
				$ext      = '.' . $request->file( 'file' )->getClientOriginalExtension();
				$fileName = $request->id . '-' . $itemName . $ext;

				// Save file to upload directory

				$request->file( 'file' )->move( $destinationPath, $fileName );

				// Orientate

				\Image::make( 'uploads/' . $fileName )->orientate()->save();

				// Resize

				$thumb = \Image::make( 'uploads/' . $fileName )->fit( 120 )->save( 'uploads/120x120/' . $fileName );

				// Save to model

				$image           = new Image;
				$image->item_id  = $item->id;
				$image->filename = $fileName;
				$image->save();

				$log          = new Log;
				$log->item_id = $item->id;
				$log->entry   = "Photo (" . $fileName . ") added by " . Auth::user()->name;
				$log->save();

			}

			if ( $request->next == 'redirect' ) {
				return redirect( '/inventory/' . $request->id . '/edit' );
			} else {
				return response()->json( [ 'success' => true ] );
			}
		}
	}

	public function delete_image(Request $request)
	{

		if (Auth::user()->can('upload_photo')) {
			$item  = Item::findOrFail( $request->id );
			$image = Image::findOrFail( $request->image_id );

			$filename = $image->filename;

			// Delete file from the filesystem

			\Storage::delete( [ 'uploads/' . $filename, 'uploads/120x120' . $filename ] );

			// Delete image db record

			$image->delete();

			// Log action

			$log          = new Log;
			$log->item_id = $item->id;
			$log->entry   = "Photo (" . $filename . ") deleted by " . Auth::user()->name;
			$log->save();


			if ( $request->next == 'redirect' ) {
				return redirect( '/inventory/' . $request->id . '/edit' );
			} else {
				return response()->json( [ 'success' => true ] );
			}
		}
		
	}
	
	public function modify_image(Request $request)
	{

		if (Auth::user()->can('upload_photo')) {
			$item  = Item::findOrFail( $request->id );
			$image = Image::findOrFail( $request->image_id );
			$type  = $request->type;

			$filename = $image->filename;

			$photo = \Image::make( 'uploads/' . $filename );
			$thumb = \Image::make( 'uploads/120x120/' . $filename );

			if ( $type == 'rotate-cw' ) {
				$photo->rotate( - 90 )->save();
				$thumb->rotate( - 90 )->save();
			}

			if ( $type == 'rotate-ccw' ) {
				$photo->rotate( 90 )->save();
				$thumb->rotate( 90 )->save();
			}

			if ( $request->next == 'redirect' ) {
				return redirect( '/inventory/' . $request->id . '/edit' );
			} else {
				return response()->json( [ 'success' => true ] );
			}
		}

	}


	public function get_logs(Request $request) {
		$item = Item::find($request->id);
		return response()->json($item->logs);
	}
}
