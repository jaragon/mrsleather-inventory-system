<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Branch;
use App\Status;
use App\Seller;
use App\Role;
use App\User;
use Validator;
use Auth;

class OptionsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$sellers = Seller::all();
		$statuses = Status::all();
		$roles = Role::all();
		$users = User::with('roles')->get();
		return view('options', compact(['sellers', 'statuses', 'roles', 'users']));
	}
	
	public function add_seller(Request $request) 
	{
		$seller = new Seller;
		$seller->name = $request->name;
		$seller->save();
		
		return redirect('/options');
	}
	
	public function delete_seller(Request $request) {
		if ( Auth::user()->can( 'manage_users' ) ) {
			$seller = Seller::find($request->id);
			$seller->delete();
			return response()->json(['success' => 'true']);
		}
	}
	
	public function add_status(Request $request)
	{
		$status = new Status;
		$status->name = $request->name;
		$status->options = $request->options;
		$status->save();

		return redirect('/options');

	}
	
	public function delete_status(Request $request) {
		if ( Auth::user()->can( 'manage_users' ) ) {
			$status = Status::find($request->id);
			$status->delete();
			return response()->json(['success' => 'true']);
		}
	}

	public function add_user(Request $request) {

		if ( Auth::user()->can( 'manage_users' ) ) {
			$this->validate( $request, [
				'name'     => 'required|max:255',
				'email'    => 'required|email|max:255|unique:users',
				'password' => 'required|min:6|confirmed',
				'role'     => 'required|numeric',
			] );

			$user = User::create( [
				'name'     => $request->name,
				'email'    => $request->email,
				'password' => bcrypt( $request->password ),
			] );

			$user->roles()->attach( $request->role );

			return redirect('/options');
		}
	}
	
	public function delete_user(Request $request) {
		if ( Auth::user()->can( 'manage_users' ) ) {
			$user = User::find($request->id);
			$user->delete();
			return response()->json(['success' => 'true']);
		}
	}
	
}
