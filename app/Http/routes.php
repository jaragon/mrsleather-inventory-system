<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'InventoryController@index');

Route::auth();

Route::get('/analytics', 'AnalyticsController@index');
Route::get('/analytics/update_date_sold', 'AnalyticsController@update_date_sold');
Route::get('/analytics/update_date_layaway', 'AnalyticsController@update_date_layaway');

Route::get('/home', 'HomeController@index');
Route::get('/options', 'OptionsController@index');
Route::post('/options/add_branch', 'OptionsController@add_branch');
Route::post('/options/add_status', 'OptionsController@add_status');
Route::post('/options/add_seller', 'OptionsController@add_seller');
Route::post('/options/add_user', 'OptionsController@add_user');
Route::post('/options/delete_user', 'OptionsController@delete_user');
Route::post('/options/delete_seller', 'OptionsController@delete_seller');
Route::post('/options/delete_status', 'OptionsController@delete_status');

Route::post('/inventory/batch_upload', 'InventoryController@batch_upload');
Route::post('/inventory/update_key', 'InventoryController@update_key');
Route::post('/inventory/upload_image', 'InventoryController@upload_image');
Route::post('/inventory/delete_image', 'InventoryController@delete_image');
Route::post('/inventory/modify_image', 'InventoryController@modify_image');
Route::post('/inventory/update_sold', 'InventoryController@update_sold');
Route::post('/inventory/update_layaway', 'InventoryController@update_layaway');
Route::post('/inventory/get_logs', 'InventoryController@get_logs');
Route::resource('inventory', 'InventoryController');

