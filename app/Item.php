<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
	public function images() 
	{
		return $this->hasMany('App\Image');
	}
	public function logs()
	{
		return $this->hasMany('App\Log');
	}
}
