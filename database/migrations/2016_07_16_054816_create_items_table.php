<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('brand')->nullable();
            $table->string('color')->nullable();
            $table->string('stored_in')->nullable();
            $table->string('sold_by')->nullable();
            $table->string('status')->nullable();
            $table->date('date_added')->nullable();
            $table->date('date_sold')->nullable();
            $table->boolean('brand_new')->nullable();
            $table->decimal('price', 5, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
