<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateItemColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->string('accessories')->nullable();
            $table->string('sold_to')->nullable();
            $table->string('notes')->nullable();
            $table->string('gender')->nullable();
            $table->string('style')->nullable();
            $table->string('size')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn([
              'accessories',
              'sold_to',
              'notes',
              'gender',
              'style',
              'size'
            ]);
        });
    }
}
