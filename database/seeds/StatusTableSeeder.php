<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::firstOrCreate(['name' => 'Available']);
        Status::firstOrCreate(['name' => 'Sold']);
        Status::firstOrCreate(['name' => 'Reserved']);
        Status::firstOrCreate(['name' => 'Lay Away']);
    }
}
