<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Roles
        $admin = Role::firstOrCreate(['name'=>'admin', 'display_name' => 'Administrator']);
        $staff = Role::firstOrCreate(['name'=>'staff', 'display_name' => 'Staff']);
        $viewer = Role::firstOrCreate(['name'=>'viewer', 'display_name' => 'Viewer']);
				$uploader = Role::firstOrCreate(['name'=>'uploader', 'display_name' => 'Uploader']);
				$client = Role::firstOrCreate(['name'=>'client', 'display_name' => 'Client']);

        $superAdmin = App\User::firstOrCreate([
          'name' => 'Administrator',
          'email' => 'jerico@thehappythreefriends.com',
          'password' => bcrypt('lalalala')
        ])->attachRoles([$admin]);

        // Permissions
        $manageUsers = Permission::firstOrCreate([
          'name' => 'manage_users',
          'display_name' => 'Manage users'
        ]);
        $manageItems = Permission::firstOrCreate([
          'name' => 'manage_items',
          'display_name' => 'Manage items'
        ]);
				$uploadPhoto = Permission::firstOrCreate([
					'name' => 'upload_photo',
					'display_name' => 'Upload Photo'
				]);
				$viewNotes = Permission::firstOrCreate([
					'name' => 'view_notes',
					'display_name' => 'View Notes'
				]);

        // Sync permissions
        $admin->perms()->sync([
          $manageUsers->id,
          $manageItems->id,
	        $uploadPhoto->id,
	        $viewNotes->id
        ]);
        $staff->perms()->sync([
          $manageItems->id,
	        $uploadPhoto->id,
	        $viewNotes->id
        ]);
				$uploader->perms()->sync([
					$uploadPhoto->id,
					$viewNotes->id
				]);
				$viewer->perms()->sync([
					$viewNotes->id
				]);


    }
}
