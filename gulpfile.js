var elixir = require('laravel-elixir');

require('laravel-elixir-stylus');

var bootstrap = require('bootstrap-styl')

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
  mix.stylus('app.styl', null, {
    use: [ bootstrap() ]
  });
  mix.scriptsIn('public/js/components', 'public/js/app.js');
  mix.scriptsIn('public/js/vendor', 'public/js/vendor.js');
  mix.browserSync({
    proxy: '127.0.0.1:8000',
    open: false,
    files: [
      'public/css/*.css',
      'public/js/*.js',
      'resources/views/**/*.blade.php',
      'app/**/*.php'
    ],
  });
});
