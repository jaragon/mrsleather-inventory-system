$(function() {
  
  var items = [];

  // Process XLSX file in-browser

  $('#xlsx-file').on('change', function(e) {
    var files = e.target.files;
    var i,f;
    for (i = 0, f = files[i]; i != files.length; ++i) {
      var reader = new FileReader();
      var name = f.name;
      reader.onload = function(e) {

        items = [];

        var data = e.target.result;

        var workbook = XLSX.read(data, {type: 'binary'});

        // Convert to JSON

        var result = {};

        workbook.SheetNames.forEach(function(sheetName) {
          var roa = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName]);
          if(roa.length > 0){
            result[sheetName] = roa;
          }
        });

        $.each(result, function(i, v) {
          $.each(v, function(i, item) {
            items.push(item);
          });
        });

        // Check if it has the right headers

        var isValid = true;

        $.each(items, function(i, v) {
          console.log(items[i].Brand);
          if (
            items[i].Description == undefined ||
            items[i].Category == undefined ||
            items[i].Brand == undefined
          ) 
          {
            isValid = false;
          }
        });

        if (isValid) {
          $('.xlsx-file-status').html('<span style="color: green">The Excel file contains: <b>' + items.length + '</b> Products</span>');
          $.each(items, function(i, v) {
            $('.xlsx-file-items').show();
            $('.xlsx-submit').show();
            $('.xlsx-file-items .panel-body').append('<p>' + v.Description + '</p>');
          });
          $('#xlsx-file + p + input').show().attr('disabled', false).val('Submit');
        } else {
          $('#xlsx-file').val('');
          $('.xlsx-file-status').html('<span style="color: red">Something is wrong with the file, please double check and try again</span>');
        }



      };
      reader.readAsBinaryString(f);
    }

  });

// Submits JSON to server for processing
  $('#xlsx-file').parents('form').on('submit', function(e) {
    e.preventDefault();

    $('.xlsx-submit').attr('disabled', true).val('Processing...');

    $.ajax({
      url: '/inventory/batch_upload',
      type: 'POST',
      dataType: 'json',
      data: {
        _token: _csrfToken,
        items: items
      },
      success: function(data) {
        $('.xlsx-submit').attr('disabled', true).val('Done!');
      }
    });

  });

});

$(function() {
  $('.js-change-log').on('click', function() {
    var $this = $(this);
    var id = $(this).data('id');
    $.ajax({
      url: '/inventory/get_logs',
      method: 'POST',
      data: {
        _token: _csrfToken,
        id: id
      },
      success: function(data) {
        var table = $('<table/>').addClass('table table-bordered table-condensed');
        $.each(data, function(i,v) {
          var tr = $('<tr/>');
          tr.append('<td>' + v.created_at + '</td>');
          tr.append('<td>' + v.entry + '</td>');
          table.append(tr);
        });
        $this.next().find('.modal-body').html(table);
        console.log(table);
      }
    })
  });
  
  
  $('.js-image-delete').on('click', function() {
    var container = $(this).parent();
    var image_id = $(this).data('image-id');
    var id = $(this).data('id');
    $.ajax({
      url: '/inventory/delete_image',
      method: 'POST',
      data: {
        _token: _csrfToken,
        image_id: image_id,
        id: id
      },
      success: function(data) {
        container.hide();
      }
    })
  });
  
  $('.js-image-modify').on('click', function() {
    var container = $(this).parent();
    var image_id = $(this).data('image-id');
    var id = $(this).data('id');
    var type = $(this).data('type');
    
    $.ajax({
      url: '/inventory/modify_image',
      method: 'POST',
      data: {
        _token: _csrfToken,
        image_id: image_id,
        id: id,
        type: type
      },
      success: function(data) {
        var d = new Date();
        var img = $(container).find('img');
        img.attr("src", img.attr('src') + "?" + d.getTime());
      }
    })
  });


});
Dropzone.autoDiscover = false;

$('.js-dropzone').each(function() {
  var $this = $(this);
  $(this).dropzone({
    url: '/inventory/upload_image',
    params: {
      '_token': _csrfToken,
      'id': $this.data('id')
    },
    previewsContainer: $this.find('.item-images')[0],
  });
});


$(function() {
  
  $('.item select').on('change', function() {
    
    var id = $(this).data('id');
    var key = $(this).attr('name');
    var value = $(this).val();
    
    if (key == 'status') {
      if (value == 'Sold') {
        $('.sold-modal').modal('show');
        $('#sold-modal-item-id').val(id);
        $('.sold-modal h4').text('Sold: ' + $('.item[data-id=' + id + '] strong').text() );
      }
      if (value == 'Lay Away') {
        $('.sold-modal').modal('show');
        $('.sold-modal').find('form').attr('action', '/inventory/update_layaway');
        $('.sold-modal input[name=sold_by]').hide().prev().hide();
        $('#sold-modal-item-id').val(id);
        $('.sold-modal h4').text('Lay Away: ' + $('.item[data-id=' + id + '] strong').text() );
      }
    }
    
    $.ajax({
      url: '/inventory/update_key',
      type: 'POST',
      dataType: 'json',
      data: {
        _token: _csrfToken,
        id: id,
        key: key,
        value: value
      },
      success: function(data) {
        console.log(data);
      }
    });
    
  });
  
  $('.filters-bar button[type=reset]').on('click', function() {
    window.location.href = '/inventory';
  });
  
});


$(function() {
  $('.js-user-delete').on('click', function() {
    var id = $(this).data('id');
    $.ajax({
      url: '/options/delete_user',
      method: 'POST',
      data: {
        _token: _csrfToken,
        id: id
      },
      success: function() {
        window.location.reload();
      }
    })
  });
  
  $('.js-seller-delete').on('click', function() {
    var id = $(this).data('id');
    $.ajax({
      url: '/options/delete_seller',
      method: 'POST',
      data: {
        _token: _csrfToken,
        id: id
      },
      success: function() {
        window.location.reload();
      }
    })
  });
  
  $('.js-status-delete').on('click', function() {
    var id = $(this).data('id');
    $.ajax({
      url: '/options/delete_status',
      method: 'POST',
      data: {
        _token: _csrfToken,
        id: id
      },
      success: function() {
        window.location.reload();
      }
    })
  });
});
//# sourceMappingURL=app.js.map
