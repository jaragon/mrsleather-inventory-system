$(function() {
  
  var items = [];

  // Process XLSX file in-browser

  $('#xlsx-file').on('change', function(e) {
    var files = e.target.files;
    var i,f;
    for (i = 0, f = files[i]; i != files.length; ++i) {
      var reader = new FileReader();
      var name = f.name;
      reader.onload = function(e) {

        items = [];

        var data = e.target.result;

        var workbook = XLSX.read(data, {type: 'binary'});

        // Convert to JSON

        var result = {};

        workbook.SheetNames.forEach(function(sheetName) {
          var roa = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName]);
          if(roa.length > 0){
            result[sheetName] = roa;
          }
        });

        $.each(result, function(i, v) {
          $.each(v, function(i, item) {
            items.push(item);
          });
        });

        // Check if it has the right headers

        var isValid = true;

        $.each(items, function(i, v) {
          console.log(items[i].Brand);
          if (
            items[i].Description == undefined ||
            items[i].Category == undefined ||
            items[i].Brand == undefined
          ) 
          {
            isValid = false;
          }
        });

        if (isValid) {
          $('.xlsx-file-status').html('<span style="color: green">The Excel file contains: <b>' + items.length + '</b> Products</span>');
          $.each(items, function(i, v) {
            $('.xlsx-file-items').show();
            $('.xlsx-submit').show();
            $('.xlsx-file-items .panel-body').append('<p>' + v.Description + '</p>');
          });
          $('#xlsx-file + p + input').show().attr('disabled', false).val('Submit');
        } else {
          $('#xlsx-file').val('');
          $('.xlsx-file-status').html('<span style="color: red">Something is wrong with the file, please double check and try again</span>');
        }



      };
      reader.readAsBinaryString(f);
    }

  });

// Submits JSON to server for processing
  $('#xlsx-file').parents('form').on('submit', function(e) {
    e.preventDefault();

    $('.xlsx-submit').attr('disabled', true).val('Processing...');

    $.ajax({
      url: '/inventory/batch_upload',
      type: 'POST',
      dataType: 'json',
      data: {
        _token: _csrfToken,
        items: items
      },
      success: function(data) {
        $('.xlsx-submit').attr('disabled', true).val('Done!');
      }
    });

  });

});
