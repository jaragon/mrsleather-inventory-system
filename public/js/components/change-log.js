$(function() {
  $('.js-change-log').on('click', function() {
    var $this = $(this);
    var id = $(this).data('id');
    $.ajax({
      url: '/inventory/get_logs',
      method: 'POST',
      data: {
        _token: _csrfToken,
        id: id
      },
      success: function(data) {
        var table = $('<table/>').addClass('table table-bordered table-condensed');
        $.each(data, function(i,v) {
          var tr = $('<tr/>');
          tr.append('<td>' + v.created_at + '</td>');
          tr.append('<td>' + v.entry + '</td>');
          table.append(tr);
        });
        $this.next().find('.modal-body').html(table);
        console.log(table);
      }
    })
  });
});