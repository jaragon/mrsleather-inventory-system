Dropzone.autoDiscover = false;

$('.js-dropzone').each(function() {
  var $this = $(this);
  $(this).dropzone({
    url: '/inventory/upload_image',
    params: {
      '_token': _csrfToken,
      'id': $this.data('id')
    },
    previewsContainer: $this.find('.item-images')[0],
  });
});

