$(function() {
  
  $('.item select').on('change', function() {
    
    var id = $(this).data('id');
    var key = $(this).attr('name');
    var value = $(this).val();
    
    if (key == 'status') {
      if (value == 'Sold') {
        $('.sold-modal').modal('show');
        $('#sold-modal-item-id').val(id);
        $('.sold-modal h4').text('Sold: ' + $('.item[data-id=' + id + '] strong').text() );
      }
    }
    
    $.ajax({
      url: '/inventory/update_key',
      type: 'POST',
      dataType: 'json',
      data: {
        _token: _csrfToken,
        id: id,
        key: key,
        value: value
      },
      success: function(data) {
        console.log(data);
      }
    });
    
  });
  
  $('.filters-bar button[type=reset]').on('click', function() {
    window.location.href = '/inventory';
  });
  
});
