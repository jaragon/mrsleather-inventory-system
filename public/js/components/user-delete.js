$(function() {
  $('.js-user-delete').on('click', function() {
    var id = $(this).data('id');
    $.ajax({
      url: '/options/delete_user',
      method: 'POST',
      data: {
        _token: _csrfToken,
        id: id
      },
      success: function() {
        window.location.reload();
      }
    })
  });
  
  $('.js-seller-delete').on('click', function() {
    var id = $(this).data('id');
    $.ajax({
      url: '/options/delete_seller',
      method: 'POST',
      data: {
        _token: _csrfToken,
        id: id
      },
      success: function() {
        window.location.reload();
      }
    })
  });
  
  $('.js-status-delete').on('click', function() {
    var id = $(this).data('id');
    $.ajax({
      url: '/options/delete_status',
      method: 'POST',
      data: {
        _token: _csrfToken,
        id: id
      },
      success: function() {
        window.location.reload();
      }
    })
  });
});