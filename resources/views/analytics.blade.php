@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-6">
                    <h2 style="margin: 0;">Analytics</h2>
                </div>
                <div class="col-md-6" style="text-align: right;">
                </div>
            </div>

            <div class="filters-bar">

                    <div class="row">
                        <div class="col-md-12">
                            <p style="opacity: 0.5">Options</p>
                        </div>
                        <div class="col-md-12">
                            <form action="/analytics" method="get" class="form-inline">
                                <!--
                                <select name="brand" class="form-control">
                                    <option value="" selected disabled>Brand</option>
                                    <option value="">All</option>
                                    @foreach($brands as $brand)
                                        @if ($request->brand == $brand->brand)
                                            <option selected value="{{ $brand->brand }}">{{ $brand->brand }}</option>
                                        @else
                                            <option value="{{ $brand->brand }}">{{ $brand->brand }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                -->
                                <select name="reportType" class="form-control">
                                    <option value="Sales" selected>Sales</option>
                                    @if ($request->reportType == 'New Items')
                                    <option value="New Items" selected>New Items</option>
                                    @else
                                    <option value="New Items">New Items</option>
                                    @endif
                                    @if ($request->reportType == 'Lay Away')
                                    <option value="Lay Away" selected>Lay away</option>
                                    @else
                                    <option value="Lay Away">Lay away</option>
                                    @endif
                                </select>
                                <select name="stored_in" class="form-control">
                                    <option value="" selected disabled>Branch</option>
                                    <option value="">All</option>
                                    @foreach($branches as $branch)
                                        @if ($request->stored_in == $branch->stored_in)
                                            <option selected>{{ $branch->stored_in }}</option>
                                        @else
                                            <option>{{ $branch->stored_in }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <!--
                                <select name="category" class="form-control">
                                    <option value="" selected disabled>Category</option>
                                    <option value="">All</option>
                                    @foreach($categories as $category)
                                        @if ($request->category == $category->category)
                                            <option selected>{{ $category->category }}</option>
                                        @else
                                            <option>{{ $category->category }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                -->
                                <!--
                                @if (isset($request->s))
                                    <input type="text" class="form-control" placeholder="Search" name="s" value="{{ $request->s }}">
                                @else
                                    <input type="text" class="form-control" placeholder="Search" name="s">
                                @endif
                                -->
                                &nbsp;&nbsp;Date Start
                                @if (isset($request->date_start))
                                    <input type="date" class="form-control" placeholder="Date Start" name="date_start" value="{{ $request->date_start }}">
                                @else
                                    <input type="date" class="form-control" placeholder="Date Start" name="date_start">
                                @endif
                                &nbsp;&nbsp;Date End
                                @if (isset($request->date_end))
                                    <input type="date" class="form-control" placeholder="Date End" name="date_end" value="{{ $request->date_end }}">
                                @else
                                    <input type="date" class="form-control" placeholder="Date End" name="date_end">
                                @endif
                                <button type="submit" class="btn btn-success">Generate</button>
                                <button type="reset" class="btn btn-danger">Reset</button>
                            </form>
                        </div>
                    </div>
            </div>

            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Item Information</th>
                    <th>Date Added</th>
                    @if ($reportType == 'Sold')
                    <th>Date Sold</th>
                    @endif
                    @if ($reportType == 'Lay Away')
                        <th>Date Lay Away</th>
                    @endif
                    <th>Price</th>
                    <th>Stored In</th>
                    <th>Sold by</th>
                    <th>Notes</th>
                    <th>Status</th>
                </tr>
                </thead>
                @foreach($items as $item)
                <tr class="item js-dropzone" data-id="{{ $item->id }}">
                    <td>
                        {{ $item->id }}
                    </td>
                    <td style="width: 20%">
                        <p>{{ $item->name }}</p>
                    </td>
                    <td>{{ date('M d, Y', strtotime($item->created_at)) }}</td>
                    @if ($reportType == 'Sold')
                    <td>
                        @if ($item->date_sold > 0)
                        {{ date('M d, Y', strtotime($item->date_sold)) }}
                        @endif
                    </td>
                    @endif
                    @if ($reportType == 'Lay Away')
                        <td>
                            @if ($item->date_layaway > 0)
                                {{ date('M d, Y', strtotime($item->date_sold)) }}
                            @endif
                        </td>
                    @endif
                    <td>{{ $item->price }}</td>
                    <td>
                        {{ $item->stored_in }}
                    </td>

                    <td>{{ $item->sold_by }}</td>
                    <td style="width: 15%">{{ $item->notes }}</td>
                    <td>
                        {{ $item->status }}
                    </td>
                </tr>
                @endforeach
            </table>


        </div>
    </div>
</div>

<div class="modal fade sold-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Sold: </h4>
            </div>
            <form action="/inventory/update_sold" method="POST">
            <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="sold_by">Sold by</label>
                                <input type="text" name="sold_by" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="notes">Notes</label>
                                <textarea name="notes" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="id" id="sold-modal-item-id" value="">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection
