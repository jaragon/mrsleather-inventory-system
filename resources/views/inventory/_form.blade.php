    {{ csrf_field() }}

    <div class="form-group">
        <label class="control-label">Name</label>
        <input type="text" name="name" value="{{ old('name', $item->name) }}" class="form-control">
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Category</label>
                <input type="text" name="category" value="{{ old('category', $item->category) }}" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Type</label>
                <input type="text" name="type" value="{{ old('type', $item->type) }}" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Brand</label>
                <input type="text" name="brand" value="{{ old('brand', $item->brand) }}" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Color</label>
                <input type="text" name="color" value="{{ old('color', $item->color) }}" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Stored In</label>
                <input type="text" name="stored_in" value="{{ old('stored_in', $item->stored_in) }}" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Brand New</label>
                <select name="brand_new" class="form-control">
                    @if (old('brand_new', $item->brand_new))
                        <option value="1" selected>New</option>
                        <option value="0">Preowned</option>
                    @else
                        <option value="1">New</option>
                        <option value="0" selected>Preowned</option>
                    @endif

                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Price</label>
                <input type="text" name="price" value="{{ old('price', $item->price) }}" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Sold By</label>
                <input type="text" name="sold_by" value="{{ old('sold_by', $item->sold_by) }}" class="form-control">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Notes</label>
                <textarea name="notes" class="form-control">{{ old('notes', $item->notes) }}</textarea>

            </div>
        </div>

    </div>

    <div class="form-group">
        @if (isset($item->id))
            <button type="submit" class="btn btn-default">
                <i class="fa fa-plus"></i> Save Changes
            </button>
        @else
            <button type="submit" class="btn btn-default">
                <i class="fa fa-plus"></i> Add Item
            </button>
        @endif
    </div>

