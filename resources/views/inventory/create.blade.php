@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-6">
                        <h2 style="margin: 0; padding-bottom: 0.5em;">Inventory</h2>
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        <a href="/inventory" class="btn btn-default">
                            Back
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h3>Batch Upload</h3>
                        <form>
                            <label for="xlsx-file" class="btn btn-default">
                                <input type='file' id='xlsx-file' accept='.xlsx' style="display: none;" />
                                Choose Excel File
                            </label>

                            <div class="xlsx-file-status"></div>

                            <div class="xlsx-file-items" style="display: none;">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Products to be processed</div>
                                    <div class="panel-body">
                                    </div>
                                </div>
                            </div>

                            <div class="xlsx-upload-status"></div>

                            <input type='submit' style="display: none;" class='btn btn-default xlsx-submit' />
                        </form>

                        <style>
                            .xlsx-file-status {
                                padding: 1em 0;
                            }
                            .xlsx-file-items .panel-body {
                                max-height: 200px;
                                overflow: scroll;
                            }
                        </style>

                    </div>
                    <div class="col-md-6 col-md-offset-3">
                        <hr>
                    </div>
                    <div class="col-md-6 col-md-offset-3">
                        <h3>Add an item</h3>
                        <form action="{{ url('inventory') }}" method="POST" class="form">
                            @include('inventory._form')
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
