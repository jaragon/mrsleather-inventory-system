@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-6">
                        <h2 style="margin: 0; padding-bottom: 0.5em;">Inventory</h2>
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        <a href="/inventory" class="btn btn-default">
                            Back
                        </a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        @permission('manage_item')
                        <form action="/inventory/{{ $item->id }}" method="POST" class="form">
                            {{ method_field('PATCH') }}
                            @include('inventory._form')
                        </form>
                        <hr>
                        @endpermission

                        @if (isset($item->id))

                            @if (count($item->images) > 0)
                                <h3>Photos</h3>
                                @foreach($item->images as $image)
                                    <div style="border: 1px solid rgba(0,0,0,0.1); padding: 1em;">

                                        @if (Auth::user()->can('upload_photo'))
                                            <div style="margin-bottom: 1em;">
                                                <button class="btn btn-default btn-xs js-image-modify" data-type="rotate-cw" data-id="{{ $item->id }}" data-image-id="{{ $image->id }}">Rotate <span class="glyphicon glyphicon-repeat"></span></button>
                                                <button class="btn btn-default btn-xs js-image-modify" data-type="rotate-ccw" data-id="{{ $item->id }}" data-image-id="{{ $image->id }}">Rotate <span style="-webkit-transform: scaleX(-1); transform: scaleX(-1)" class="glyphicon glyphicon-repeat"> </button>
                                                <button class="btn btn-default btn-xs js-image-delete" data-id="{{ $item->id }}" data-image-id="{{ $image->id }}" style="float: right;">Delete</button>
                                            </div>
                                        @endif

                                        <img style="max-width: 100%;" src="/uploads/{{ $image->filename }}" alt="">
                                    </div>
                                @endforeach
                                <hr>
                            @endif


                            <h3>Upload Photo</h4>

                            <form action="/inventory/upload_image" method="POST" class="form" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="file" name="file" />
                                <input type="hidden" name="id" value="{{ $item->id }}" />
                                <input type="hidden" name="next" value="redirect" />
                                <br>
                                <button type="submit" class="btn btn-default">Upload</button>
                            </form>

                            <hr>

                            <h4>Change Log</h4>
                            <table class="table table-condensed table-bordered table-responsive">
                                @foreach($item->logs as $log)
                                    <tr>
                                        <td>{{ $log->created_at }}</td>
                                        <td>{!! $log->entry !!}</td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif

                        @role('admin')
                        @if (isset($item->id))
                            <hr>

                            <form action="/inventory/{{ $item->id }}" method="POST" class="form">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <div class="form-group">
                                    <button type="submit" class="btn btn-danger">
                                        <i class="fa fa-minus"></i> Delete Item
                                    </button>
                                </div>
                            </form>

                        @endif
                        @endrole
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
