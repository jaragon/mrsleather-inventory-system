@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-6">
                    <h2 style="margin: 0;">Inventory</h2>
                </div>
                <div class="col-md-6" style="text-align: right;">
                    @permission('manage_items')
                    <a href="/inventory/create" class="btn btn-default">
                        <i class="fa fa-plus"></i> Add Items
                    </a>
                    @endpermission
                </div>
            </div>

            <div class="filters-bar">

                    <div class="row">
                        <div class="col-md-12">
                            <p style="opacity: 0.5">Filters</p>
                        </div>
                        <div class="col-md-12">
                            <form action="/inventory" method="get" class="form-inline">
                                <select name="brand" class="form-control">
                                    <option value="" selected disabled>Brand</option>
                                    <option value="">All</option>
                                    @foreach($brands as $brand)
                                        @if ($request->brand == $brand->brand)
                                            <option selected value="{{ $brand->brand }}">{{ $brand->brand }}</option>
                                        @else
                                            <option value="{{ $brand->brand }}">{{ $brand->brand }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <select name="stored_in" class="form-control">
                                    <option value="" selected disabled>Branch</option>
                                    <option value="">All</option>
                                    @foreach($branches as $branch)
                                        @if ($request->stored_in == $branch->stored_in)
                                            <option selected>{{ $branch->stored_in }}</option>
                                        @else
                                            <option>{{ $branch->stored_in }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <select name="category" class="form-control">
                                    <option value="" selected disabled>Category</option>
                                    <option value="">All</option>
                                    @foreach($categories as $category)
                                        @if ($request->category == $category->category)
                                            <option selected>{{ $category->category }}</option>
                                        @else
                                            <option>{{ $category->category }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <select name="status" class="form-control">
                                    <option value="" selected disabled>Status</option>
                                    <option value="">All</option>
                                    @foreach($statuses as $status)
                                        @if ($request->status == $status->name)
                                            <option selected>{{ $status->name }}</option>
                                        @else
                                            <option>{{ $status->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if (isset($request->s))
                                    <input type="text" class="form-control" placeholder="Search" name="s" value="{{ $request->s }}">
                                @else
                                    <input type="text" class="form-control" placeholder="Search" name="s">
                                @endif
                                <button type="submit" class="btn btn-success">Filter</button>
                                <button type="reset" class="btn btn-danger">Reset</button>
                            </form>
                        </div>
                    </div>
            </div>

            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Item Information</th>
                    <th>Stored In</th>
                    <th>Status</th>
                    @permission('view_notes')
                    <th></th>
                    @endpermission
                </tr>
                </thead>
                @foreach($items as $item)
                <tr class="item js-dropzone" data-id="{{ $item->id }}">
                    <td>
                        {{ $item->id }}
                    </td>
                    <td style="width: 50%">
                        <p><strong>{{ $item->name }}</strong></p>
                        <table class="item-info">
                            <tr>
                                <td>Date Added</td>
                                <td>{{ date('F d, Y', strtotime($item->created_at)) }}</td>
                            </tr>
                            <tr>
                                <td>Brand</td>
                                <td>{{ $item->brand }}</td>
                            </tr>
                            <tr>
                                <td>Category</td>
                                <td>{{ $item->category }}</td>
                            </tr>
                            @if ( ! empty($item->type) )
                            <tr>
                                <td>Type</td>
                                <td>{{ $item->type }}</td>
                            </tr>
                            @endif

                            @if ( ! empty($item->brand_new) )
                                <tr>
                                    <td>Brand New</td>
                                    <td>
                                        @if ($item->brand_new)
                                            New
                                        @else
                                            Preowned
                                        @endif
                                    </td>
                                </tr>
                            @endif

                            @if ( ! empty($item->price) )
                            <tr>
                                <td>Price</td>
                                <td>{{ number_format($item->price, 2) }}</td>
                            </tr>
                            @endif

                            @if ( ! empty($item->color) )
                            <tr>
                                <td>Color</td>
                                <td>{{ $item->color }}</td>
                            </tr>
                            @endif

                            @if ( ! empty($item->accessories) )
                                <tr>
                                    <td>Accessories</td>
                                    <td>{{ $item->accessories }}</td>
                                </tr>
                            @endif

                            @if ( ! empty($item->gender) )
                                <tr>
                                    <td>Gender</td>
                                    <td>{{ $item->gender }}</td>
                                </tr>
                            @endif

                            @if ( ! empty($item->style) )
                                <tr>
                                    <td>Style</td>
                                    <td>{{ $item->style }}</td>
                                </tr>
                            @endif

                            @if ( ! empty($item->size) )
                                <tr>
                                    <td>Size</td>
                                    <td>{{ $item->size }}</td>
                                </tr>
                            @endif

                            @if ( ! empty($item->sold_to) )
                                <tr>
                                    <td>Sold to</td>
                                    <td>{{ $item->sold_to }}</td>
                                </tr>
                            @endif

                            @if ( ! empty($item->sold_by) )
                                <tr>
                                    <td>Sold by</td>
                                    <td>{{ $item->sold_by }}</td>
                                </tr>
                            @endif

                            @if ( ! empty($item->date_sold) )
                                <tr>
                                    <td>Date Sold</td>
                                    <td>{{ date('F d, Y', strtotime($item->date_sold)) }}</td>
                                </tr>
                            @endif

                            @permission('view_notes')
                                @if ( ! empty($item->notes) )
                                    <tr>
                                        <td>Notes</td>
                                        <td>{{ $item->notes }}</td>
                                    </tr>
                                @endif
                            @endpermission

                        </table>
                        <div class="item-images">
                            @foreach($item->images as $image)
                                <div class="dz-preview">
                                    <a href="/uploads/{{ $image->filename }}" data-lightbox="item-{{ $item->id }}" class="dz-image">
                                        <img src="/uploads/120x120/{{ $image->filename }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </td>
                    <td>
                        @if (Auth::user()->can('manage_items'))
                        <select name="stored_in" style="height: 22px" class="form-control" data-id="{{ $item->id }}">
                            <option value=""></option>
                            @foreach($branches as $branch)
                                @if ($branch->stored_in == $item->stored_in)
                                    <option value="{{ $branch->stored_in }}" selected>{{ $branch->stored_in }}</option>
                                @else
                                    <option value="{{ $branch->stored_in }}">{{ $branch->stored_in }}</option>
                                @endif
                            @endforeach
                        </select>
                        @else
                            {{ $item->stored_in }}
                        @endif
                    </td>

                    <td>
                        @if (Auth::user()->can('manage_items') && $item->status != 'Sold')
                            <select name="status" style="height: 22px" class="form-control" data-id="{{ $item->id }}">
                                <option value=""></option>
                                @foreach($statuses as $status)
                                    @if ($status->name == $item->status)
                                        <option value="{{ $status->name }}" selected>{{ $status->name }}</option>
                                    @else
                                        <option value="{{ $status->name }}">{{ $status->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        @elseif (Auth::user()->can('manage_users') && $item->status == 'Sold')
                            <select name="status" style="height: 22px" class="form-control" data-id="{{ $item->id }}">
                                <option value=""></option>
                                @foreach($statuses as $status)
                                    @if ($status->name == $item->status)
                                        <option value="{{ $status->name }}" selected>{{ $status->name }}</option>
                                    @else
                                        <option value="{{ $status->name }}">{{ $status->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        @else
                            {{ $item->status }}
                        @endif
                    </td>

                    @permission('view_notes')
                    <td style="text-align: right">
                        <button class="btn btn-default btn-xs js-change-log" data-id="{{ $item->id }}" data-toggle="modal" data-target="#item-{{ $item->id }}-log"><i class="fa fa-info-circle"></i> Change Log</button>
                        <div class="modal fade" id="item-{{ $item->id }}-log" tabindex="-1" role="dialog" style="text-align: left;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">{{ $item->name }}</h4>
                                    </div>
                                    <div class="modal-body">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @permission('upload_photo')
                        <a href="/inventory/{{ $item->id }}/edit" class="btn btn-default btn-xs"><i class="fa fa-info-circle"></i> Edit</a>
                        @endpermission
                    </td>
                    @endif
                </tr>
                @endforeach
            </table>

            {{ $items->appends($request->except('page'))->links() }}


        </div>
    </div>
</div>

<div class="modal fade sold-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Sold: </h4>
            </div>
            <form action="/inventory/update_sold" method="POST">
            <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="sold_by">Sold by</label>
                                <input type="text" name="sold_by" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="notes">Notes</label>
                                <textarea name="notes" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="id" id="sold-modal-item-id" value="">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection
