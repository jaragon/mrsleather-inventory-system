@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @role('admin')

            <div class="panel panel-default">
                <div class="panel-heading">Manage Users</div>
                <div class="panel-body">
                    @if (count($roles) > 0)
                        <h4>Users</h4>
                        <table class="table table-striped">
                            <tr>
                                <th><strong>Name</strong></th>
                                <th><strong>Access Rights</strong></th>
                                <th></th>
                            </tr>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>
                                    @foreach($user->roles as $role)
                                        {{ $role->display_name }}
                                    @endforeach
                                </td>
                                <td style="text-align: right;">
                                    @if (Auth::user()->id != $user->id)
                                        <button class="btn btn-danger btn-xs js-user-delete" data-id="{{ $user->id }}">Delete</button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        <br>
                    @endif

                    <h4>Add User</h4>
                    <form action="/options/add_user" method="POST" class="form">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="control-label">Name</label>
                            <input type="text" name="name" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="text" name="email" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input type="password" name="password" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="control-label">Confirm Password</label>
                            <input type="password" name="password_confirmation" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="control-label">Access Rights</label>
                            <select name="role" class="form-control">
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->display_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default">
                                <i class="fa fa-plus"></i> Add User
                            </button>
                        </div>
                    </form>

                </div>
            </div>

            @endrole
        </div>

        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Sellers</div>
                <div class="panel-body">
                    @if (count($sellers) > 0)
                    <table class="table table-striped">
                        @foreach($sellers as $seller)
                            <tr>
                                <td>
                                    {{ $seller->name }}
                                </td>
                                <td style="text-align: right;">
                                    <button class="btn btn-danger btn-xs js-seller-delete" data-id="{{ $seller->id }}">Delete</button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <br>
                    @endif

                    <h4>Add Sellers</h4>
                    <form action="/options/add_seller" method="POST" class="form">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="control-label">Seller Name</label>
                            <input type="text" name="name" class="form-control">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default">
                                <i class="fa fa-plus"></i> Add Seller
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Item Status</div>
                <div class="panel-body">
                    @if (count($statuses) > 0)
                        <table class="table table-striped">
                            @foreach($statuses as $status)
                                <tr>
                                    <td>
                                        {{ $status->name }}
                                    </td>
                                    <td style="text-align: right;">
                                        <button class="btn btn-danger btn-xs js-status-delete" data-id="{{ $status->id }}">Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <br>
                    @endif


                    <h4>Add status</h4>
                    <form action="/options/add_status" method="POST" class="form">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="control-label">Status Name</label>
                            <input type="text" name="name" class="form-control">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default">
                                <i class="fa fa-plus"></i> Add Status
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>
</div>
@endsection
